#!/usr/bin/python3

# This program is free software; you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation; either version 3 of the License, or (at your option) any
# later version.  See http://www.gnu.org/copyleft/lgpl.html for the full text
# of the license.


import dbus
import dbusmock
import getpass
import gi
import os
import subprocess
import sys
import tempfile
import unittest

gi.require_version('Gtk', '3.0')
from gi.repository import GLib, Gio, Gtk

ALL_FPRINTD_POLICYKIT_RULES = [
    'net.reactivated.fprint.device.setusername',
    'net.reactivated.fprint.device.enroll',
    'net.reactivated.fprint.device.verify'
]

VALID_FINGER_NAMES = [
    'left-thumb',
    'left-index-finger',
    'left-middle-finger',
    'left-ring-finger',
    'left-little-finger',
    'right-thumb',
    'right-index-finger',
    'right-middle-finger',
    'right-ring-finger',
    'right-little-finger'
]

ENROLL_STATES = {
    'enroll-completed': True,
    'enroll-failed': True,
    'enroll-stage-passed': False,
    'enroll-retry-scan': False,
    'enroll-swipe-too-short': False,
    'enroll-finger-not-centered': False,
    'enroll-remove-and-retry': False,
    'enroll-data-full': True,
    'enroll-disconnected': True,
    'enroll-unknown-error': True,
}

class FakeAccountsServiceMock:
    BUS_NAME = 'org.freedesktop.Accounts'
    MAIN_OBJ = '/org/freedesktop/Accounts'
    MAIN_IFACE = 'org.freedesktop.Accounts'
    SYSTEM_BUS = True

    @staticmethod
    def get_user_path(user):
        return '/org/freedesktop/Accounts/User{}'.format(user)

    @staticmethod
    def load(mock, parameters={}):
        mock.AddMethods(FakeAccountsServiceMock.MAIN_IFACE, [
            (
                'ListCachedUsers',
                '',
                'ao',
                'ret = {}'.format([FakeAccountsServiceMock.get_user_path(u) for u in parameters['users'].keys()])
            ),
        ], dbus_interface=dbusmock.MOCK_IFACE)

        for uid, name in parameters['users'].items():
            mock.AddObject(FakeAccountsServiceMock.get_user_path(uid),
                FakeAccountsServiceMock.MAIN_IFACE + '.User', {
                    'Uid': dbus.UInt64(uid),
                    'UserName': name,
                    'RealName': name[0].upper() + name[1:] + ' Fake',
                    'AccountType': dbus.Int32(1),
                    'HomeDirectory': '/tmp/home/%s' % name,
                    'Shell': '/bin/bash',
                    'Email': '',
                    'Language': 'C',
                    'FormatsLocale': 'C',
                    'InputSources':  dbus.Array([], signature='(ss)'),
                    'XSession': 'ubuntu',
                    'Location': '',
                    'LoginFrequency': dbus.UInt64(0),
                    'LoginTime': dbus.Int64(0),
                    'LoginHistory': dbus.Array([], signature='(xxa{sv})'),
                    'XHasMessages': False,
                    'XKeyboardLayouts': dbus.Array([], signature='s'),
                    'BackgroundFile': '',
                    'IconFile': '',
                    'Locked': False,
                    'PasswordMode': 0,
                    'PasswordHint': '',
                    'AutomaticLogin': False,
                    'SystemAccount': False,
                    'LocalAccount': True,
                }, [], dbus_interface=dbusmock.MOCK_IFACE)


class FakeFprintd(object):
    '''Test fprintd utilities'''

    def __init__(self):
        super().__init__()

        self.users = {}
        self.fprintd_mock = None
        self.devices_mocks = []

        self.test_bus = Gio.TestDBus.new(Gio.TestDBusFlags.NONE)
        self.test_bus.up()
        self.test_bus.unset()
        addr = self.test_bus.get_bus_address()
        os.environ['DBUS_SYSTEM_BUS_ADDRESS'] = addr
        self.dbus = Gio.DBusConnection.new_for_address_sync(addr,
            Gio.DBusConnectionFlags.MESSAGE_BUS_CONNECTION |
            Gio.DBusConnectionFlags.AUTHENTICATION_CLIENT, None, None)

        self.dbus_mock = dbusmock.DBusTestCase()
        self.dbus_con = self.dbus_mock.get_dbus(system_bus=True)

        self._polkitd, self.polkitd_obj = self.dbus_mock.spawn_server_template(
            'polkitd', {}, stdout=subprocess.DEVNULL)
        self.set_pkit_fprint_rules(ALL_FPRINTD_POLICYKIT_RULES)

        self.polkitd_obj.Check

        self._logind, self._logind_obj = self.dbus_mock.spawn_server_template(
            'logind', {}, stdout=subprocess.DEVNULL)
        self._logind_obj.AddSeat('seat0')
        self.add_user(os.getuid(), getpass.getuser())
        self.add_user(111, 'other-unlogged-user')

        session_path = self._logind_obj.AddSession('session0', 'seat0',
            os.getuid(), self.get_myself_name(), True)

        session_object = self.dbus_con.get_object(self._logind_obj.bus_name, session_path)
        session_props = dbus.Interface(session_object, dbus_interface='org.freedesktop.DBus.Properties')
        session_props.Set('org.freedesktop.login1.Session', 'Type',
            'x11' if 'DISPLAY' in os.environ else 'wayland')

        self._fdo_accounts = self.dbus_mock.spawn_server(
            FakeAccountsServiceMock.BUS_NAME, FakeAccountsServiceMock.MAIN_OBJ,
            FakeAccountsServiceMock.MAIN_IFACE, FakeAccountsServiceMock.SYSTEM_BUS,
            stdout=subprocess.PIPE)

        self._fdo_accounts_obj = self.dbus_con.get_object(
            FakeAccountsServiceMock.BUS_NAME, FakeAccountsServiceMock.MAIN_OBJ)
        FakeAccountsServiceMock.load(self._fdo_accounts_obj, {'users': self.users})

        self.start_fprintd()

    def start_fprintd(self):
        if self.fprintd_mock:
                return

        if 'FPRINTD_MOCK_PATH' in os.environ:
            fprintd_template = os.path.join(os.getenv('FPRINTD_MOCK_PATH'), 'fprintd.py')
        else:
            fprintd_template = os.path.join(os.path.dirname(__file__), 'dbusmock/fprintd.py')
        print ('Using template from %s' % fprintd_template)

        self.fprintd_mock, self.obj_fprintd_manager = self.dbus_mock.spawn_server_template(
            fprintd_template, {}, stdout=subprocess.PIPE)

        return self.fprintd_mock

    def stop_fprintd(self):
        if not self.fprintd_mock:
            return
        self.fprintd_mock.terminate()
        self.fprintd_mock.wait()
        self.obj_fprintd_manager = None
        self.fprintd_mock = None
        self.devices_mocks = []

    def add_user(self, uid, name):
        self.users[uid] = name
        self._logind_obj.AddUser(uid, name, uid == os.getuid())

    def get_myself_name(self):
        return self.users[os.getuid()]

    def add_device(self, enroll_stages=3, name=None):
        if not name:
            name = 'Fake fprint device {}'.format(len(self.devices_mocks) + 1)
        device_path = self.obj_fprintd_manager.AddDevice(name, enroll_stages, 'swipe')
        device_mock = self.dbus_con.get_object('net.reactivated.Fprint', device_path)
        self.devices_mocks.append(device_mock)
        return device_mock

    def remove_device(self, device_mock):
        if device_mock in self.devices_mocks:
            self.obj_fprintd_manager.RemoveDevice(device_mock.object_path)
            self.devices_mocks.remove(device_mock)

    def set_enrolled_fingers(self, device, fingers=[], user=None):
        user = user if user else self.get_myself_name()
        device.SetEnrolledFingers(user, dbus.Array(set(fingers), signature='s'))

    def get_enrolled_fingers(self, device, user=None):
        user = user if user else self.get_myself_name()
        try:
            return device.ListEnrolledFingers(user)
        except Exception as e:
            if 'net.reactivated.Fprint.Error.NoEnrolledPrints' in str(e):
                return []
            raise(e)

    def get_device_prop(self, device_mock, prop):
        dev_props = dbus.Interface(device_mock, dbus_interface='org.freedesktop.DBus.Properties')
        return dev_props.Get('net.reactivated.Fprint.Device', prop)

    def set_pkit_fprint_rules(self, allowed_rules):
        print('Allowed rules set to ',allowed_rules)
        self.pkit_fprint_rules = allowed_rules
        self.polkitd_obj.SetAllowed(self.pkit_fprint_rules +
            ['org.gnome.controlcenter.user-accounts.administration'])


class FprintdController(Gtk.Window):

    def __init__(self, daemon):
        Gtk.Window.__init__(self, title="Fprintd controller")
        self.set_border_width(10)
        self.set_accept_focus(False)
        self.stick()

        self.daemon = daemon

        vbox = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=10)
        vbox.set_valign(Gtk.Align.START)
        self.main_vbox = vbox
        self.main_vbox.connect('size-allocate', lambda w, a: self.resize_window())
        self.add(vbox)

        hbox = Gtk.Box(spacing=5)
        vbox.add(hbox)

        delay_vbox = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=5)
        delay_vbox.add(Gtk.Label.new('Server delay (ms)'))
        hbox.add(delay_vbox)
        self.server_delay = Gtk.SpinButton()
        self.server_delay.set_increments(+100, -100)
        self.server_delay.set_range(0, 5000)
        self.server_delay.set_value(0)
        self.server_delay.connect('value-changed',
            lambda w: self.daemon.obj_fprintd_manager.SetDBusDelayMs(w.get_value()))
        delay_vbox.add(self.server_delay)

        rules_vbox = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=5)
        rules_vbox.add(Gtk.Label.new('Policykit rules:'))
        hbox.add(rules_vbox)
        self.rules_hbox = Gtk.Box(spacing=3)
        rules_vbox.add(self.rules_hbox)
        for rule in ALL_FPRINTD_POLICYKIT_RULES:
            check = Gtk.CheckButton.new_with_label(
                rule.replace('net.reactivated.fprint.device.', ''))
            check.set_active(rule in self.daemon.pkit_fprint_rules)
            check.connect('toggled', self.on_rule_changed)
            self.rules_hbox.add(check)

        self.dev_vbox = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
        self.dev_vbox.set_valign(Gtk.Align.START)
        self.dev_vbox.add(Gtk.FlowBox()) # bah, without this first dev would be ugly
        vbox.add(self.dev_vbox)

        hbox = Gtk.Box(spacing=6)
        vbox.add(hbox)

        self.add_device_button = Gtk.Button.new_with_mnemonic("Add _device")
        self.add_device_button.connect("clicked", lambda w: self.add_device_ui())
        hbox.add(self.add_device_button)

        self.n_enrolls = Gtk.SpinButton()
        self.n_enrolls.set_placeholder_text('Enroll stages')
        self.n_enrolls.set_increments(+1, -1)
        self.n_enrolls.set_range(1, 100)
        self.n_enrolls.set_value(5)
        hbox.add(self.n_enrolls)

        self.dev_name = Gtk.Entry()
        self.dev_name.set_placeholder_text('Device name')
        self.dev_name.set_input_purpose(Gtk.InputPurpose.NUMBER)
        hbox.add(self.dev_name)

        button = Gtk.Button.new_with_mnemonic("Run d-_feet")
        button.connect("clicked", lambda w: subprocess.call(
            ['d-feet'], env=os.environ))
        vbox.add(button)

        self.fprintd_button = Gtk.Button.new_with_mnemonic("Stop fprintd")
        self.fprintd_button.connect("clicked", lambda w: self.manage_fprintd())
        vbox.add(self.fprintd_button)

        gcc_hbox = Gtk.Box(spacing=3)
        vbox.add(gcc_hbox)
        self.gcc = None
        self.gcc_button = Gtk.Button.new_with_mnemonic("Run Gnome _control-center")
        self.gcc_button.connect("clicked", lambda w: self.manage_gnome_control_center())
        gcc_hbox.add(self.gcc_button)

        gcc_options = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
        gcc_hbox.add(gcc_options)

        self.gcc_interactive_debug = Gtk.CheckButton.new_with_label('Interactive debug')
        gcc_options.add(self.gcc_interactive_debug)

        self.gcc_msg_all = Gtk.CheckButton.new_with_label('All debug messages')
        gcc_options.add(self.gcc_msg_all)

        self.gcc_valgrind = Gtk.CheckButton.new_with_label('Run in Valgrind')
        gcc_options.add(self.gcc_valgrind)

        self.lang_c = Gtk.CheckButton.new_with_label('Don\'t translate')
        gcc_options.add(self.lang_c)

        self.fatal_criticals = Gtk.CheckButton.new_with_label('Fatal criticals')
        gcc_options.add(self.fatal_criticals)

        gcc_options.show_all()

        self.connect('notify::is-active', lambda w, p: self.reload_devices_state())

    def manage_fprintd(self):
        if self.daemon.fprintd_mock:
            self.daemon.stop_fprintd()
            self.fprintd_button.set_label('Start fprintd')
            self.add_device_button.set_sensitive(False)
        else:
            self.daemon.start_fprintd()
            self.fprintd_button.set_label('Stop fprintd')
            self.add_device_button.set_sensitive(True)
            self.daemon.obj_fprintd_manager.SetDBusDelayMs(self.server_delay.get_value())

    def manage_gnome_control_center(self):
        if not self.gcc:
            cmd = ['gnome-control-center', 'user-accounts']
            if os.getenv('VALGRIND') or self.gcc_valgrind.get_active():
                suppressions = '{}/share/glib-2.0/valgrind/glib.supp'.format(
                    subprocess.check_output(['pkg-config', 'glib-2.0', '--variable=prefix']).decode('utf-8').strip())
                cmd = ['valgrind', '--leak-check=full', '--suppressions={}'.format(suppressions)] + cmd

            environ = os.environ.copy()
            if self.gcc_interactive_debug.get_active():
                environ['GTK_DEBUG'] = 'interactive'

            if self.gcc_msg_all.get_active():
                environ['G_MESSAGES_DEBUG'] = 'all'

            if self.lang_c.get_active():
                environ['LANG'] = 'C'

            if self.fatal_criticals.get_active():
                environ['G_DEBUG'] = 'fatal-criticals'

            self.gcc = subprocess.Popen(cmd, env=environ)
            self.gcc_button.set_label('_Kill gnome-control-center')
        else:
            self.gcc.terminate()
            self.gcc.wait()
            self.gcc = None
            for device in self.daemon.devices_mocks:
                if device.enroll_actions_box:
                    device.enroll_actions_box.hide()
                    self.resize_window()
                    try:
                        device.EnrollStop()
                    except:
                        pass

                try:
                    device.Release()
                except:
                    pass
            self.gcc_button.set_label("Run Gnome _control-center")

    def on_rule_changed(self, w):
        rules = []
        for toggle in self.rules_hbox.get_children():
            if isinstance(toggle, Gtk.CheckButton) and toggle.get_active():
                rules.append('net.reactivated.fprint.device.{}'.format(
                    toggle.get_label()))
        self.daemon.set_pkit_fprint_rules(rules)

    def on_device_signal(self, proxy, sender, signal, params):
        print(signal, params)

        if signal == 'DebugStatus':
            status, _status_params = params.unpack()
            if status == 'EnrollStart':
                proxy.device.enroll_actions_box.show_all()
            elif status == 'EnrollStop':
                proxy.device.enroll_actions_box.hide()
                self.resize_window()
            elif status.startswith('DeleteEnrolledFingers'):
                self.reload_devices_state()
        if signal == 'EnrollStatus':
            self.reload_devices_state()

    def resize_window(self):
        _min_req, nat_req = self.main_vbox.get_preferred_size()
        if nat_req.width > 0 and nat_req.height > 0:
            self.resize(nat_req.width, nat_req.height)

    def remove_device_ui(self, device):
        device.proxy.disconnect(device.g_signal_id)
        device.proxy.disconnect(device.g_name_owner_id)
        device.proxy = None
        self.daemon.remove_device(device)
        self.dev_vbox.remove(device.vbox)
        self.resize_window()

    def device_name_owner_changed(self, device_proxy, pspec):
        if not device_proxy.get_name_owner():
            self.remove_device_ui(device_proxy.device)

    def add_device_ui(self):
        vbox = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
        vbox.set_valign(Gtk.Align.START)

        device = self.daemon.add_device(enroll_stages=self.n_enrolls.get_value(),
            name=self.dev_name.get_text())
        device.vbox = vbox

        hbox = Gtk.Box()
        vbox.add(hbox)
        label = Gtk.Label.new()
        label.set_markup('<b>{}</b>'.format(self.daemon.get_device_prop(device, 'name')))
        hbox.add(label)

        button = Gtk.Button.new_with_label("Remove")
        button.connect('clicked', lambda b: self.remove_device_ui(b.device))
        button.device = device
        hbox.pack_end(button, False, False, 0)

        device.fingers_flow = Gtk.FlowBox()
        device.fingers_flow.set_selection_mode(Gtk.SelectionMode.NONE)
        device.fingers_flow.set_max_children_per_line(len(VALID_FINGER_NAMES))
        device.fingers_flow.set_focus_on_click(False)
        device.fingers_flow.set_valign(Gtk.Align.START)
        vbox.add(device.fingers_flow)
        enrolled_prints = self.daemon.get_enrolled_fingers(device)

        for finger in VALID_FINGER_NAMES:
            check = Gtk.CheckButton.new_with_label(finger.replace('-finger', ''))
            check.set_active(finger in enrolled_prints)
            check.connect('toggled', self.on_finger_changed)
            check.device = device
            check.finger = finger
            child = Gtk.FlowBoxChild()
            child.check = check
            child.add(check)
            device.fingers_flow.add(child)

        vbox.show_all()

        device_proxy = Gio.DBusProxy.new_sync(self.daemon.dbus,
            Gio.DBusProxyFlags.DO_NOT_AUTO_START, None,
            'net.reactivated.Fprint', device.object_path,
            'net.reactivated.Fprint.Device',
            None)

        device_proxy.device = device
        device.g_signal_id = device_proxy.connect('g-signal', self.on_device_signal)
        device.g_name_owner_id = device_proxy.connect('notify::g-name-owner',
            self.device_name_owner_changed)
        device.proxy = device_proxy

        print('Added device ' + device_proxy.get_cached_property('name').unpack())

        device.enroll_actions_box = Gtk.FlowBox()
        device.enroll_actions_box.set_selection_mode(Gtk.SelectionMode.NONE)
        device.enroll_actions_box.set_focus_on_click(False)
        vbox.add(device.enroll_actions_box)

        for state in ENROLL_STATES.keys():
            button = Gtk.Button.new_with_label(state.replace('enroll-', ''))
            button.state = state
            button.connect('clicked', lambda w: device.EmitEnrollStatus(w.state, ENROLL_STATES[w.state]))
            device.enroll_actions_box.add(button)

        self.dev_vbox.add(vbox)

    def on_finger_changed(self, check):
        enrolled = self.daemon.get_enrolled_fingers(check.device)
        if check.get_active():
            enrolled.append(check.finger)
        elif check.finger in enrolled:
            enrolled.remove(check.finger)
        self.daemon.set_enrolled_fingers(check.device, enrolled)

    def reload_devices_state(self):
        for device in self.daemon.devices_mocks:
            enrolled_prints = self.daemon.get_enrolled_fingers(device)
            for child in device.fingers_flow.get_children():
                child.check.set_active(child.check.finger in enrolled_prints)



if __name__ == '__main__':
    # avoid writing to stderr
    daemon = FakeFprintd()
    controller = FprintdController(daemon)

    print('Runing fprintd in ',os.environ['DBUS_SYSTEM_BUS_ADDRESS'])

    controller.connect("destroy", Gtk.main_quit)
    controller.show_all()
    Gtk.main()
